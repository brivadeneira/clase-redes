# Clase de Redes de datos

Fundamentos básicos de redes, reseña histórica, configuración básica de red en GNU/Linux.

Incluye:

- Apunte en pdf.
- Presentación de la clase en SVG.
- Configuración paso a paso en GNU/Linux en asciimatics.

Última clase de la cátedra de Introducción a la Ing. en Telecomunicaciones de la UNRC.
